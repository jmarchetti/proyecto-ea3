# Servidor para control de temperatura
## Prerrequisitos
Las aplicaciones que componen a este proyecto están containerizadas bajo docker, por lo que es necesario instalarlo para levantar los servicios. Instalar docker según las instrucciones del [sitio oficial](https://docs.docker.com/engine/install/ubuntu/).

## Instalación
Para levantar todos los servicios, ubicarse en el root del repositorio y ejecutar:

    docker-compose up -d

## Configuración del despliegue
En caso de necesitar modificar algún puerto, o alguna variable de entorno de algún servicio, se puede modificar el archivo `docker-compose.yaml`, que contiene la declaración de todos los servicios a levantar.

## Modificación y/o personalización de imágenes
Si se desea aplicar una modificación en el código de backend o de la gui, se debe construir o "buildear" una nueva imagen. Para ello, se pueden usar los siguientes comandos (desde el root del repositorio, en el sistema operativo Linux):

    # Para el backend
    cd backend && docker build . -t temp-server-backend:1.0
    # Para la gui
    cd temp-gui && docker build . -t temp-server-gui:1.0

Luego se debe modificar esas imágenes en el archivo `docker-compose.yaml` y volver a ejecutar `docker-compose up -d`.
