import "./App.css";
import Body from "./components/Body"

export default function App() {
  return (
    <div className="App">
      <section className="App-content">
        <h1>Control de temperatura</h1>
        {/* <img src={logo} className="App-logo" alt="logo" /> */}
        {/* <img src={fan} className="fan-img" alt="fan" /> */}
        <Body />
      </section>
    </div>
  );
}
