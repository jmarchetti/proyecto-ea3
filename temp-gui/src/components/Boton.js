import { useState } from "react"
import './Boton.css'

export default function Boton({title, api}){
    const [status, setStatus] = useState(false)
    const [showStatus, setShowStatus] = useState(false)
    const handleClick = () => {
        fetch(api)
        .then(response => {
            if (response.status === 200)
                setStatus(true)
            else
                setStatus(false)
            setShowStatus(true)
            setTimeout(() => {
                setShowStatus(false)
            }, 5000)
        })
    }
    const buttonClassName = showStatus ? (status ? "btn-status-ok" : "btn-status-error") : '';
    return (
        <>
            <button className={`btn ${buttonClassName}`} onClick={handleClick}>{title}</button>
        </>
    )
}