import React from 'react'
import InfoFromApi from './InfoFromApi'
import Control from './Control'
import Dashboard from './Dashboard'
import Fan from './Fan'
import './Body.css'

const BACKEND_URL = "/api"

export default function Body(){
    return(
        <div className='body-container'>
            <InfoFromApi
                title="Temperatura"
                apiget={`${BACKEND_URL}/get_last_temp`}
            />
            <Fan
                api={`${BACKEND_URL}/get_last_fan`}
            />
            <Control />
            <Dashboard />
        </div>
    )
}