import React, {useState} from 'react'
import Fan from './Fan'
import Configure from './Configure'
import Boton from './Boton'
import './Control.css'

const BACKEND_URL = "/api"

export default function Control(){
    const [autEnabled, setAutEnabled] = useState(false)
    const handleManual = () => {
        setAutEnabled(false)
    }
    const handleAutomatic = () => {
        setAutEnabled(true)
        fetch(`${BACKEND_URL}/toggle_automatic`)
    }
    return(
        <div className="control-container">
            <div className="control-btn-container">
                <button onClick={handleManual} className={`control-btn ${!autEnabled ? 'control-btn-selected' : ''}`}>Manual</button>
                <button onClick={handleAutomatic} className={`control-btn ${autEnabled ? 'control-btn-selected' : ''}`}>Automático</button>
            </div>
            <div className="control-info-container">
            {autEnabled ? 
                <>
                    <Configure 
                        title="Máximo"
                        api={`${BACKEND_URL}/set_tmax/`}
                        initial=''
                    />
                    <Configure 
                    title="Histéresis"
                    api={`${BACKEND_URL}/set_hist/`}
                    initial=''
                    />
                </>
             :
                <>
                    <Boton 
                        title='On'
                        api={`${BACKEND_URL}/set_fan/on`}
                    />
                    <Boton 
                        title='Off'
                        api={`${BACKEND_URL}/set_fan/off`}
                    />
                </>
              }
            </div>
        </div>
    )
}