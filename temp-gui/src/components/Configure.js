import React, {useState} from 'react';
import './Configure.css'

export default function Configure ({title, api, initial}){
    const [value, setValue] = useState('')
    const [status, setStatus]   = useState(true);
    const [showStatus, setShowStatus] = useState(false);
    const handleChange = (event) =>{
        setValue(event.target.value);
    }
    const handleSet = () => {
        console.log("Fetching", api + value)
        fetch(api + value)
            .then((response) => {
                console.log(response)
                if (response.status === 200){
                    console.log(response)
                    setStatus(true);
                    setShowStatus(true);
                    setTimeout(() => {
                        setShowStatus(false);
                    }, 5000)
                }
                else{
                    setStatus(false);
                    setShowStatus(true);
                    setTimeout(() => {
                        setShowStatus(false);
                    }, 5000)
                }
            })
            .catch((error) => console.error(error))
    }
    const buttonClassName = showStatus ? (status ? "btn-status-ok" : "btn-status-error") : '';
    return (
        <div className='configure-container'>
            <label className='configure-label'>{title}</label>
            <input className='configure-input' type='number' step='1' min='0' onChange={handleChange} value={value}/>
            <button className={`configure-btn ${buttonClassName}`} onClick={handleSet}>Actualizar</button>
        </div>
    )
}