import React, {useState, useEffect} from 'react'
import './InfoFromApi.css'

export default function InfoFromApi({title, apiget}){
    const [info, setInfo] = useState("");
    // Update value every 10s
    useEffect(()=> {
        setInterval(() => {
            fetch(apiget)
                .then((response) => response.json())
                .then((json) => {
                    setInfo(()=>json.response)
                })
                .catch((error) => console.error(error))
        }, 2000)

    }, [apiget]);
    return (
        <div className="info">
            <span className="info-title">{title}</span>
            <span className="info-value">{info} °C</span>
        </div>
    )    
}