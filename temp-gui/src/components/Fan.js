import React, {useState, useEffect} from 'react'
import fan from '../fan.svg'
import './Fan.css'

export default function Fan({api}){
    const mapResponse = {
        "0":false,
        "1":true
    }
    const [fanStatus, setFanStatus] = useState(false);
    useEffect(()=> {
        setInterval(() => {
            fetch(api)
                .then((response) => response.json())
                .then((json) => {
                    setFanStatus(()=>json.response==="1" ? true : false)
                })
                .catch((error) => console.error(error))
        }, 2000)

    }, [api]);
    return (
        <div className='fan-container'>
            <img src={fan} className={`fan-img ${fanStatus ? "fan-on" : ""}`} alt="fan"/>
        </div>
    )
}