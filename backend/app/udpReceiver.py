import socket, logging, sys
from influxdb import InfluxDBClient
# from os import getenv
from functions import LOG_LEVEL, UDP_IP_LOCAL, UDP_PORT_LOCAL, UDP_LOCAL_SERVER_PORT
# udp influx inserter
# udpReceiver code

# Escucha en un puerto UDP determinado por env var, y cuando recibe el string correspondiente a temperatura,
# lo inserta en influx como una measurement de temperatura. Si el string es distinto a la temperatura, lo 
# lo reenvía al puerto 12000 para que lo escuche el server.

def udpReceiver():
    logger = logging.getLogger("udpReceiver")
    logger.setLevel(LOG_LEVEL)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(LOG_LEVEL)
    formatter = logging.Formatter('[%(asctime)s] %(name)s - %(levelname)s -> %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    TEMP_STRING = b"T="
    FAN_STRING = b",F="

    point = [
        {
            "measurement": "Temperatura",
            "tags":
            {
                "temp":"temp"
            },
            "fields":
            {
                "temperatura": 25
            },
        },
        {
            "measurement": "Fan",
            "tags":
            {
                "fan":"fan"
            },
            "fields":
            {
                "fan": 1
            },
        }
    ]

    client = InfluxDBClient(host="influxdb", port=8086)
    client.switch_database("temp_data")
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    logger.info(f'Listening UDP on {UDP_IP_LOCAL}:{UDP_PORT_LOCAL}')
    sock.bind((UDP_IP_LOCAL, UDP_PORT_LOCAL))
    while True:
        data, addr = sock.recvfrom(1024)
        logger.debug(f"Received udp msg: {data}")
        indexTemp = data.find(TEMP_STRING)
        indexFan = data.find(FAN_STRING)
        if indexTemp > -1 and indexFan > -1:
            dataTemp = data[(indexTemp+len(TEMP_STRING)):indexFan]
            strTemp = str(dataTemp, 'utf-8')
            temp = int(strTemp)
            logger.info(f"Received temp: {temp}")
            dataFan = data[(indexFan+len(FAN_STRING)):]
            strFan = str(dataFan, 'utf-8')
            fan = int(strFan)
            logger.info(f"Received fan: {fan}")
            point[0]['fields']['temperatura'] = temp
            point[1]['fields']['fan'] = fan
            client.write_points(point)
            logger.debug(f"Writing to influx: -> {point}")
        else:
            # Reenvío al server porque no es temperatura lo que me manda
            logger.info("It's not temperature. Proxying to server...")
            sock.sendto(data, ('backend', UDP_LOCAL_SERVER_PORT))

if __name__ == '__main__':
    udpReceiver()