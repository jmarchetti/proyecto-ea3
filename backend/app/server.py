# Server code

# Flask backend server code

# Endpoints:
#   - /get_last_temp -> devuelve el valor last de la temperatura
#   - /get_mean_temp -> devuelve el promedio de influx
#   - /toggle_automatic
#   - /set_vent_on
#   - /set_vent_off
#   - /set_tmax
#   - /set_hist

from flask import Flask, json, jsonify
from flask_cors import CORS, cross_origin
from influxdb import InfluxDBClient

import functions, logging, sys

INFLUX_QUERY_LAST = 'SELECT last("temperatura") FROM "Temperatura"'
INFLUX_QUERY_MEAN = 'SELECT mean("temperatura") FROM "Temperatura"'
INFLUX_QUERY_FAN = 'SELECT last("fan") FROM "Fan"'

client = InfluxDBClient(host='influxdb', port=8086)
client.switch_database('temp_data')

# create and configure the app
app = Flask(__name__, instance_relative_config=True)
cors = CORS(app , resources={r"/*": {"origins": "*", "allow_headers": "*", "expose_headers": "*"}})



app.logger = logging.getLogger('server')
app.logger.setLevel(functions.LOG_LEVEL)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(functions.LOG_LEVEL)
formatter = logging.Formatter('[%(asctime)s] %(name)s - %(levelname)s -> %(message)s')
handler.setFormatter(formatter)
app.logger.addHandler(handler)
# a simple page that says hello
@app.route('/health')
def health():
  return "It's working!", 200

@app.route('/get_last_fan')
def getLastFan():
  result = client.query(INFLUX_QUERY_FAN)
  lastFan = 0
  for point in result.get_points():
    lastFan = point['last']
    app.logger.info(f"Last fan is {lastFan}")
    return jsonify(
      {
        'response' : f'{lastFan}' 
      }
    ), 200

@app.route('/get_last_temp')
def getLastTemp():
  result = client.query(INFLUX_QUERY_LAST)
  lastTemp = 0
  for point in result.get_points():
    lastTemp = point['last']
  app.logger.info(f"Last is {lastTemp}")
  return jsonify(
    {
      'response': "{:.2f}".format(lastTemp)
    }
  ), 200

@app.route('/get_mean_temp')
def getMeanTemp():
  result = client.query(INFLUX_QUERY_MEAN)
  for point in result.get_points():
    meanTemp = point['mean']
  app.logger.info(f"Mean is {meanTemp}")
  return jsonify(
    {
      'response' : "{:.2f}".format(meanTemp)
    }
  ), 200

@app.route('/toggle_automatic')
def toggleAutomatic():
  functions.cmd_automatic()
  return "ok", 200

@app.route('/set_fan/<status>')
def setFan(status):
  app.logger.info(f"Setting fan {status}")
  functions.cmd_setFan(status)
  return "ok", 200

@app.route('/set_tmax/<tmax>')
def setTmax(tmax):
  try:
    tmax = int(tmax)
    app.logger.info(f"Setting new tmax: {tmax}")
    functions.cmd_setTmax(tmax)
    return "ok", 200
  except Exception as e:
    app.logger.error(f"Error: {e}")
    return "error", 500

@app.route('/set_hist/<hist>')
def setHist(hist):
  try:
    hist = int(hist)
    app.logger.info(f"Setting new hist: {hist}")
    functions.cmd_setHist(hist)
    return "ok", 200
  except Exception as e:
    app.logger.error(f"Error: {e}")
    return "error", 500


def server(port):
  app.run(host='0.0.0.0', port=port)

if __name__ == '__main__':
  server(5000)