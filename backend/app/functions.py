# Este archivo contiene los flujos de comunicación de cada endpoint con el ESP8266

import socket
from os import getenv
from logging import INFO, DEBUG, WARNING, ERROR
from influxdb import InfluxDBClient

UDP_IP_LOCAL = getenv('UDP_IP_LOCAL', default="0.0.0.0")
UDP_PORT_LOCAL = getenv('UDP_PORT_LOCAL', default='10000')
UDP_PORT_LOCAL = int(UDP_PORT_LOCAL)
ESP_UDP_IP = getenv('ESP_UDP_IP', default="127.0.0.1") # ESP IP
ESP_UDP_PORT = getenv('ESP_UDP_PORT', default= '10000')
ESP_UDP_PORT = int(ESP_UDP_PORT)
LOG_LEVEL = getenv('LOG_LEVEL', default=DEBUG)

UDP_LOCAL_SERVER_PORT = getenv('UDP_LOCAL_SERVER_PORT', default='12000')
UDP_LOCAL_SERVER_PORT = int(UDP_LOCAL_SERVER_PORT)

# Lista de comandos

CMD_CAMBIAR_TMAX = b'cambiarTmax'
CMD_CAMBIAR_HIST = b'cambiarBrecha'
CMD_SET_FAN_ON = b'on'
CMD_SET_FAN_OFF = b'off'
CMD_CONTROL = b'control'

def cmd_setTmax(tmax):
    message = CMD_CAMBIAR_TMAX
    intTmax = int(tmax)
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((UDP_IP_LOCAL, UDP_LOCAL_SERVER_PORT)) # para escuchar
        sock.sendto(message, (ESP_UDP_IP, ESP_UDP_PORT))
        response, addr = sock.recvfrom(1024)
        response = response.decode('utf-8')
        print(f"---------- Response: {response}")
        if response == 'ok':
            message = str(intTmax).encode('utf-8')
            sock.sendto(message, (ESP_UDP_IP, ESP_UDP_PORT))
        sock.close()


def cmd_setHist(hist):
    message = CMD_CAMBIAR_HIST
    intHist = int(hist)
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((UDP_IP_LOCAL, UDP_LOCAL_SERVER_PORT)) # para escuchar
        sock.sendto(message, (ESP_UDP_IP, ESP_UDP_PORT))
        response, addr = sock.recvfrom(1024)
        response = response.decode('utf-8')
        print(f"---------- Response: {response}")
        if response == 'ok':
            message = str(intHist).encode('utf-8')
            sock.sendto(message, (ESP_UDP_IP, ESP_UDP_PORT))
        sock.close()

def cmd_setFan(status):
    if status == 'on':
        message = CMD_SET_FAN_ON
        fanValue = 1
    elif status == 'off':
        message = CMD_SET_FAN_OFF
        fanValue = 0
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.sendto(message, (ESP_UDP_IP, ESP_UDP_PORT))
    client = InfluxDBClient(host="influxdb", port=8086)
    client.switch_database("temp_data")
    point = [
        {
            "measurement": "Fan",
            "tags":
            {
                "fan":"fan"
            },
            "fields":
            {
                "fan": fanValue
            },
        }
    ]
    client.write_points(point)
    

def cmd_automatic():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.sendto(CMD_CONTROL, (ESP_UDP_IP, ESP_UDP_PORT))
