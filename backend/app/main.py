import threading
from server import server
from udpReceiver import udpReceiver
from os import getenv

serverPort = getenv('API_SERVER_PORT', default= '5000')
serverPort = int(serverPort)

if __name__ == '__main__':
    
    udpReceiverThread = threading.Thread(group=None, target=udpReceiver)
    apiServerThread = threading.Thread(group=None, target=server, args=(serverPort,))
    udpReceiverThread.start()
    apiServerThread.start()