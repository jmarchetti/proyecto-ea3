import socket
import sys

UDP_IP_LOCAL = "127.0.0.1"
UDP_PORT_LOCAL = 10000
UDP_IP_REMOTE = "192.168.0.9"
UDP_PORT_REMOTE = 8001

MESSAGE = sys.argv[1].encode('utf-8')


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.sendto(MESSAGE, (UDP_IP_REMOTE, UDP_PORT_REMOTE))