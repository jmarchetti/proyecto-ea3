/* Copyright 2016, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief This is a simple blink example.
 */

/** \addtogroup blink Bare-metal blink example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "main.h"
#include "board.h"
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include "ciaaTick.h"
#include "ciaaUART.h"
#include "ciaaIO.h"
#include "esp8266_driver.h"

/*==================[macros and definitions]=================================*/

#define BUF_LEN 512
#define STR_BUF_LEN 1024

// P2_10, LED1, puedo usarlo con la libreria Board
#define MOTOR_PORT 6
#define MOTOR_PIN 1
#define MOTOR_GPIO_PORT 3
#define MOTOR_GPIO_PIN 0

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);
static void adcInit(void);

/** @brief delay function
 * @param t desired milliseconds to wait
 */

/*==================[internal data definition]===============================*/

static uint16_t adc_temp;
static uint8_t Tmax = 35;
static uint8_t Brecha = 5;
static uint8_t Tbandera = 0; //no enviar T

static char buffer[BUF_LEN];

static uint8_t forzarON = 0;
static uint8_t forzarOFF = 0;
static uint8_t fanStatus = 0;
static uint32_t pausems_cont = 1;
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void initHardware(void) {
	Board_Init();
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / 1000);

	ciaaTickInit();
	ciaaIOInit();
	ciaaUARTInit();

    /*Pin de salida al motor*/
    /* EDU-CIAA LED1 (P2_10 pin) configuration */
    Chip_SCU_PinMux(MOTOR_PORT, MOTOR_PIN, SCU_MODE_INACT | SCU_MODE_ZIF_DIS, SCU_MODE_FUNC0);
//   	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MOTOR_PORT, MOTOR_PIN);
    Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MOTOR_GPIO_PORT, MOTOR_GPIO_PIN);
 	// Turn LED1 off [UM:19.5.3.8]
    Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, MOTOR_GPIO_PORT, MOTOR_GPIO_PIN);
}

static void adcInit(void) {
	ADC_CLOCK_SETUP_T adc;
	Chip_ADC_Init(LPC_ADC0, &adc);
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH1, ENABLE);
	Chip_ADC_SetSampleRate(LPC_ADC0, &adc, 88000);

	Chip_ADC_SetBurstCmd(LPC_ADC0, ENABLE);
}

void SysTick_Handler(void)
{
    static int ledcont = 0;
    //static uint32_t Tcont = 0;

    ledcont++;
    //Tcont++;
    if(ledcont == 200)
    {
        ciaaToggleOutput(LED3);
        ledcont = 0;
    }

    /*Bandera de envio de Temperatura cada 10 segundos*/
   // if(Tcont == 1000 ){
    //	Tcont = 0;
    	//Tbandera = 1; //enviar T
    //}
    if(pausems_cont) descontar();




}

/*==================[external functions definition]==========================*/

int main(void) {
	int i = 0, rv;
	unsigned int acum = 0;
	char str[STR_BUF_LEN];
	espStatus_e s;

	initHardware();
	adcInit();

	do {
		s = espInit(ESP_STATION);
		//pausems(100);
	} while ((s != ESP_NO_CHANGE) && (s != ESP_OK));

	if ((s == ESP_NO_CHANGE) || (s == ESP_OK)) {
		uartSend(CIAA_UART_USB, "ESP Ready!\r\n", strlen("ESP Ready!\r\n"));

		printf("Connecting...\r\n");
		uartSend(CIAA_UART_USB, "Connecting...\r\n",
				strlen("Connecting...\r\n"));
		//pausems(100);

		s = espConnectToAP("TEST", "testpassword");

		if (s == ESP_OK) {
			bzero(buffer, BUF_LEN);
			s = espGetIP(buffer, BUF_LEN);
			if (s == ESP_OK) {

				snprintf(str, STR_BUF_LEN, "Done! IP: %s", buffer);
				uartSend(CIAA_UART_USB, str, strlen(str));
				//pausems(100);

				s = espStartUDP("192.168.0.14", 10000, 8001);
				if (s == ESP_OK) {
					uartSend(CIAA_UART_USB, "Sending to port 10000\r\n",
							strlen("Sending to port 8000\r\n"));
				} else {
					snprintf(str, STR_BUF_LEN, "Error %d :(\r\n", s);
					uartSend(CIAA_UART_USB, str, strlen(str));

				}
			}
		} else {

			snprintf(str, STR_BUF_LEN, "Error %d :(\r\n", s);
			uartSend(CIAA_UART_USB, str, strlen(str));
		}
	} else {

		snprintf(str, STR_BUF_LEN, "Error %d :(\r\n", s);
		uartSend(CIAA_UART_USB, str, strlen(str));
	}

	while (1) {
		pausems(DELAY_MS);

		Board_LED_Toggle(LED);
		bzero(buffer, BUF_LEN);
		bzero(str, STR_BUF_LEN);
		rv = 0;
		rv = espGetData(buffer, BUF_LEN);
		if (rv > 0) {
			/*Recibi de la UART*/
			buffer[rv] = 0;
			snprintf(str, STR_BUF_LEN, "Se recibieron %u bytes: %s.\r\n", rv,
					buffer);
			uartSend(CIAA_UART_USB, str, strlen(str));
			if (!strcmp(buffer, "on")) {
				forzarON = 1;
				forzarOFF = 0;
				espSendData("ok on", strlen("ok on"));
				pausems(100);
			}
			if (!strcmp(buffer, "off")) {
				forzarON = 0;
				forzarOFF = 1;
				espSendData("ok off", strlen("ok off"));
				pausems(100);
			}
			if (!strcmp(buffer, "control")) {
				forzarON = 0;
				forzarOFF = 0;
				espSendData("ok control", strlen("ok control"));
				pausems(100);
			}
			if (!strcmp(buffer, "cambiarTmax")) {
				espSendData("ok", strlen("ok"));
				pausems(100);
				bzero(buffer, BUF_LEN);
				rv = 0;

				while (rv <= 0) {
					pausems(500);
					rv = espGetData(buffer, BUF_LEN);
				}
				buffer[rv] = 0;

				snprintf(str, STR_BUF_LEN, "rv = %d", rv);
				uartSend(CIAA_UART_USB, str, strlen(str));

				snprintf(str, STR_BUF_LEN, "atoi = %d", atoi(buffer));
				uartSend(CIAA_UART_USB, str, strlen(str));

				buffer[rv] = 0;
				if ((atoi(buffer) > 0) && (atoi(buffer) < 100)) {
					Tmax = atoi(buffer);
					snprintf(str, STR_BUF_LEN, "Tmax = %d", Tmax);
					uartSend(CIAA_UART_USB, str, strlen(str));
					espSendData("ok Tmax", strlen("ok Tmax"));
					pausems(100);
				}
				bzero(str, STR_BUF_LEN);
				snprintf(str, STR_BUF_LEN, "Tmax = %d", Tmax);
				uartSend(CIAA_UART_USB, str, strlen(str));
			}

			if (!strcmp(buffer, "cambiarBrecha")) {
				espSendData("ok", strlen("ok"));
				pausems(100);
				bzero(buffer, BUF_LEN);
				rv = 0;
				while (rv <= 0) {
					rv = espGetData(buffer, BUF_LEN);
				}
				buffer[rv] = 0;
				if ((atoi(buffer) > 0) && (atoi(buffer) < 100)) {
					Brecha = atoi(buffer);
					espSendData("ok Brecha", strlen("ok Brecha"));
					pausems(100);
				}
			}
		}
		while (i < 100) {
			while (Chip_ADC_ReadStatus(LPC_ADC0, ADC_CH1, ADC_DR_DONE_STAT)
					!= SET)
				;
			Chip_ADC_ReadValue(LPC_ADC0, ADC_CH1, &adc_temp);
			acum += adc_temp;
			i++;
			pausems(2);
		}
		adc_temp = acum / i;
		adc_temp = (uint16_t) ((adc_temp * 330) / 1024);
		i = 0;
		acum = 0;

		if ((adc_temp >= Tmax || forzarON) && (!forzarOFF)) {
			//prender motor;
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, MOTOR_GPIO_PORT, MOTOR_GPIO_PIN);
			Board_LED_Set(LED1, 1);
			fanStatus = 1;
		}
		if ((adc_temp <= (Tmax - Brecha) || forzarOFF) && (!forzarON)) {
			//apagar motor;
			Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, MOTOR_GPIO_PORT, MOTOR_GPIO_PIN);
			Board_LED_Set(LED1, 0);
			fanStatus = 0;
		}

		Tbandera++;
		if (Tbandera >= 14) {
			Tbandera = 0;
			bzero(str, 500);
			snprintf(str, 500, "T=%d,F=%d", adc_temp, fanStatus);
			uartSend(CIAA_UART_USB, str, strlen(str));
			espSendData(str, strlen(str));
		}
	}
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
