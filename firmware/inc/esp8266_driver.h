
#ifndef _ESP8266_DRIVER_H_
#define _ESP8266_DRIVER_H_


/*==================[inclusions]=============================================*/

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum
{
    ESP_BUF_FULL    = -3,
    ESP_TIMEOUT     = -2,
    ESP_ERROR       = -1,
    ESP_OK          =  0,
    ESP_NO_CHANGE   =  1,
    ESP_READY       =  2
}espStatus_e;

typedef enum
{
    ESP_STATION     = 1,
    ESP_AP          = 2,
    ESP_AP_STATION  = 3
}espMode_e;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

espStatus_e espInit(espMode_e m);
espStatus_e espListAP(char * rsp, int rsplen);
espStatus_e espConnectToAP(char * ssid, char * pw);
espStatus_e espStartUDP(char * url, uint16_t port, uint16_t localport);
espStatus_e espGetIP(char * ip, int len);
int espGetData(void * data, int len);
espStatus_e espCommand(char * cmd, char * rsp, int rsplen);
int32_t espSendData(void * data, int32_t len);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* _ESP8266_DRIVER_H_ */
