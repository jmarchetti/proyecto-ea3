from random import random
from time import sleep
from os import getenv
import socket

UDP_IP_SERVER = getenv('UDP_IP_SERVER', '127.0.0.1')
UDP_PORT_SERVER = getenv('UDP_PORT_SERVER', '10000')
UDP_PORT_SERVER = int(UDP_PORT_SERVER)

def tempSender():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    mean_value = 25
    while True:
        value = mean_value + (random() - 0.5) * 10   # Que varíe +- 5 grados desde mean_value
        value = int(value*10)
        if random() > 0.5:
            fanValue = 1
        else:
            fanValue = 0
        # fanValue = 0
        message = f"T={value},F={fanValue}".encode('utf-8')
        print(f"Sending {message} to udpReceiver")
        sock.sendto(message, (UDP_IP_SERVER, UDP_PORT_SERVER))
        sleep(10)