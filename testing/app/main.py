import threading
from tempSender import tempSender
from ESP_Tester import ESP_Tester
from os import getenv

TESTER_LOCAL_PORT = getenv('TESTER_LOCAL_PORT', default= '13000')
TESTER_LOCAL_PORT = int(TESTER_LOCAL_PORT)

if __name__ == '__main__':
    
    tempSenderThread = threading.Thread(group=None, target=tempSender)
    ESP_TesterThread = threading.Thread(group=None, target=ESP_Tester, args=(TESTER_LOCAL_PORT,))
    tempSenderThread.start()
    ESP_TesterThread.start()