import socket
def ESP_Tester(TESTER_LOCAL_PORT):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('0.0.0.0', TESTER_LOCAL_PORT))
    while True:
        data, addr = sock.recvfrom(1024)
        remoteIp = addr[0]
        remotePort = addr[1]
        command = str(data, 'utf-8').rstrip()
        print(f"Received Command: {command}")
        if command == "cambiarTmax":
            message = b'ok'
            print(f"Sending ok")
            sock.sendto(message, (remoteIp, remotePort))
            data, addr = sock.recvfrom(1024)
            print(f"New Tmax is {str(data, 'utf-8')}")
        elif command == "cambiarBrecha":
            message = b'ok'
            sock.sendto(message, (remoteIp, remotePort))
            print(f"Sending ok")
            data, addr = sock.recvfrom(1024)
            print(f"New THist is {str(data, 'utf-8')}")
        elif command == "on":
            message = b'ok'
            sock.sendto(message, (remoteIp, remotePort))
            print(f"Sending ok")
            print(f"Fan turned ON")
        elif command == "off":
            message = b'ok'
            print(f"Sending ok")
            sock.sendto(message, (remoteIp, remotePort))
            print(f"Fan turned OFF")
        elif command == "control":
            message = b'ok'
            print(f"Sending ok")
            sock.sendto(message, (remoteIp, remotePort))
            print(f"Automatic control toggled")
        pass
    pass